import React, {createContext, Component} from 'react';

const TodoContext = createContext();

class TodoProvider extends Component {
  // Context state
  state = {
    todos: {},
  };

  // Update state
  setTodo = (todo) => {
    this.setState((prevState) => ({todos}));
  };

  render() {
    const {children} = this.props;
    const {todos} = this.state;
    const {setTodo} = this;

    return (
      <TodoContext.Provider value={{todos, setTodo}}>
        {children}
      </TodoContext.Provider>
    );
  }
}

export {TodoProvider};
// export const TodoProvider = TodoContext.Provider;
// export const TodoConsumer = TodoContext.Consumer;

export default TodoContext;
