import React, {Component, useContext} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Button,
} from 'react-native';
import TodoContext from '../../context/context';

const Header = (props) => (
  <View style={styles.header}>
    <Text style={styles.headerText}>To Do List App</Text>
  </View>
);

const InputForm = () => (
  <View style={styles.inputPart}>
    <Text style={styles.infoText}>Masukkan To do List:</Text>
    <View style={styles.inputDiv}>
      <TextInput
        placeholder="Input new task"
        style={styles.inputTask}
        clearTextOnFocus
        onChangeText={() => null}
      />
      <TouchableOpacity style={styles.tombolInput} onPress={() => null}>
        <Text style={styles.tombolText}>+</Text>
      </TouchableOpacity>
    </View>
  </View>
);

// const TodoList = () => {
//   const todo = useContext(TodoContext);

//   return (
//     <View>
//       <Header />
//       <InputForm />
//       <View style={styles.body}>
//         {todo.map((item) => (
//           <Text style={styles.itemList}>{item.todo}</Text>
//         ))}
//       </View>
//     </View>
//   );
// };

class TodoList extends Component {
  static contextType = TodoContext;

  render() {
    const {todos, setTodo} = this.context;

    return (
      <View>
        <Header />
        <InputForm />
        <View style={styles.body}>
          <Button
            onPress={() => {
              const newTodo = {id: 3, todo: 'new todo', completed: false};
              setTodo(newTodo);
            }}
            title="Tambah"
            style={styles.button}
          />
          <Text>{`To do baru: ${todos.todo}`}</Text>
        </View>
      </View>
      // <div>
      //   <button
      //     onClick={() => {
      //       const newUser = { name: 'Joe', loggedIn: true }

      //       setUser(newUser)
      //     }}
      //   >
      //     Update User
      //   </button>
      //   <p>{`Current User: ${user.name}`}</p>
      // </div>
    );
  }
}

export default TodoList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  header: {
    height: 40,
    backgroundColor: '#27AE60',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  infoText: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  inputPart: {
    height: 100,
    backgroundColor: 'white',
  },
  inputDiv: {
    flexDirection: 'row',
    marginRight: 10,
    marginVertical: 10,
  },
  inputTask: {
    height: 40,
    marginHorizontal: 10,
    backgroundColor: 'white',
    flex: 6,
    borderColor: 'grey',
    borderWidth: 0.5,
    borderRadius: 5,
    color: 'gray',
  },
  tombolInput: {
    width: 40,
    height: 40,
    flex: 1,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  tombolText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  itemSeparator: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  listEmpty: {
    fontSize: 20,
  },
  todoList: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  todoItemText: {
    fontSize: 14,
  },
  body: {
    marginTop: 10,
    flex: 1,
  },
  itemList: {
    height: 60,
    paddingHorizontal: 20,
    marginVertical: 5,
    alignItems: 'center',
    backgroundColor: 'yellow',
  },
  button: {
    marginTop: 10,
    width: '80%',
    height: 40,
    backgroundColor: 'green',
  },
});
