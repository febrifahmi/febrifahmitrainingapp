import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const Index = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Account</Text>
      </View>
      <View style={styles.userProfile}>
        <Image
          source={{
            uri:
              'https://pbs.twimg.com/profile_images/2661212504/dc29df654d8957df954db40db2a50d7f_400x400.jpeg',
          }}
          style={styles.imageProfile}
        />
        <Text style={styles.userProfileText}>Febri Fahmi Hakim</Text>
      </View>
      <View style={styles.body}>
        <View style={styles.saldo}>
          <TouchableOpacity style={styles.wallet}>
            <View style={styles.walletInside}>
              <Icon
                style={styles.icons}
                name="wallet"
                size={30}
                color="#2C3E50"
              />
              <Text>Saldo</Text>
            </View>

            <View>
              <Text>Rp. 120.000.000</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.settingsTop}>
            <Icon
              style={styles.icons}
              name="settings"
              size={30}
              color="#2C3E50"
            />
            <Text>Pengaturan</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.settings}>
            <Icon
              style={styles.icons}
              name="help-circle"
              size={30}
              color="#2C3E50"
            />
            <Text>Bantuan</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.settings}>
            <Icon
              style={styles.icons}
              name="md-newspaper"
              size={30}
              color="#2C3E50"
            />
            <Text>Syarat & Ketentuan</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity style={styles.settingsTop}>
            <Icon style={styles.icons} name="exit" size={30} color="#2C3E50" />
            <Text>Keluar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Index;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
  },
  header: {
    width: '100%',
    height: 50,
    backgroundColor: '#27AE60',
    elevation: 5,
  },
  headerText: {
    marginTop: 10,
    marginHorizontal: 20,
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  userProfile: {
    height: 80,
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: '#D5D8DC',
    borderBottomWidth: 0.5,
  },
  userProfileText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#2C3E50',
  },
  imageProfile: {
    marginVertical: 10,
    marginHorizontal: 20,
    width: 48,
    height: 48,
    borderRadius: 24,
  },
  body: {
    flex: 9,
  },
  wallet: {
    height: 60,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomColor: '#D5D8DC',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
  },
  walletInside: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  saldo: {
    justifyContent: 'space-between',
  },
  settingsTop: {
    marginTop: 5,
    paddingHorizontal: 20,
    height: 60,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomColor: '#D5D8DC',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
  },
  settings: {
    paddingHorizontal: 20,
    height: 60,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomColor: '#D5D8DC',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
  },
  icons: {
    marginRight: 10,
  },
});
