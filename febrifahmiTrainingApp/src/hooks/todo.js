import React, {useState} from 'react';
import {View, Text} from 'react-native';

let mytodos = [
  {
    id: 1,
    todo: 'Coba react native',
    completed: true,
  },
  {
    id: 2,
    todo: 'Masak bumbu dapur',
    completed: true,
  },
];

const useToDo = (initialValue = mytodos) => {
  const [todo, setToDo] = useState(initialValue);

  return {
    todo,
    addTodo: (text) => {
      setToDo([
        ...todo,
        {
          id: new Date().getTime(),
          text,
          completed: false,
        },
      ]);
    },
    toggleTodo: (id) =>
      setToDo(
        todo.map((todoitem) =>
          todoitem.id === id
            ? {...todoitem, completed: !todoitem.completed}
            : todoitem,
        ),
      ),
    removeTodo: (id) => setToDo(todo.filter((todoitem) => todoitem.id !== id)),
  };
};

export default useToDo;
