import {useState} from 'react';

const useInput = (initialValue = '') => {
  const [inputValue, setInputValue] = useState(initialValue);

  return {
    inputValue,
    changeInput: (text) => setInputValue(text),
    clearInput: () => setInputValue(''),
  };
};

export default useInput;
