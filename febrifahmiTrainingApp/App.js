import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
// import TodoContext from './src/context/context';


// import Tugas1 from './Tugas1/Tugas1';
// import Index from './Tugas2/index';
// import TodoList from './Tugas3/TodoList';
// import TodoApp from './src/screens/Tugas4/index';
import AppNavigation from './src/screens/Tugas6/routes';

const App = () => {
  return (
    <View style={styles.container}>
      {/* <Tugas1 /> */}
      {/* <StatusBar barStyle="light-content" backgroundColor="#27AE60" /> */}
      {/* <Index /> */}
      {/* <TodoApp /> */}
      {/* <TodoContext /> */}
      <AppNavigation />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
  },
});

export default App;
