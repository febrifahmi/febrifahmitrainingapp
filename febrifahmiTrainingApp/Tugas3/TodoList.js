/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import useInput from '../src/hooks/form';
import useToDo from '../src/hooks/todo';

const Header = (props) => (
  <View style={styles.header}>
    <Text style={styles.headerText}>To Do List App</Text>
  </View>
);

const InputForm = ({inputValue, changeInput, clearInput, addTodo}) => (
  <View style={styles.inputPart}>
    <Text style={styles.infoText}>Masukkan To do List:</Text>
    <View style={styles.inputDiv}>
      <TextInput
        placeholder="Input new task"
        style={styles.inputTask}
        clearTextOnFocus
        onChangeText={(text) => changeInput(text)}
        value={inputValue}
      />
      <TouchableOpacity
        style={styles.tombolInput}
        onPress={() => {
          addTodo(inputValue);
          clearInput();
        }}>
        <Text style={styles.tombolText}>+</Text>
      </TouchableOpacity>
    </View>
  </View>
);

const ToDoItem = ({item, onPressItem, removeTodo}) => (
  <View style={styles.todoList}>
    <TouchableOpacity onPress={() => onPressItem(item.id)}>
      <Text
        style={{
          fontSize: 14,
          textDecorationLine: item.completed ? 'line-through' : 'none',
        }}>
        {item.text}
      </Text>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => removeTodo(item.id)}>
      <Icon name="trash" size={32} />
    </TouchableOpacity>
  </View>
);

const ToDo = ({todo, toggleTodo, removeTodo}) => (
  <FlatList
    data={todo}
    renderItem={({item}) => (
      <ToDoItem item={item} onPressItem={toggleTodo} removeTodo={removeTodo} />
    )}
    keyExtractor={(item, index) => item.id.toString()}
    listEmptyComponent={() => (
      <View style={styles.listEmpty}>
        <Text>BELUM ADA TASK</Text>
      </View>
    )}
    itemSeparatorComponent={() => <View style={styles.itemSeparator} />}
  />
);

const TodoList = () => {
  const {todo, toggleTodo, addTodo, removeTodo} = useToDo();
  const {inputValue, changeInput, clearInput} = useInput();
  return (
    <View style={styles.container}>
      <Header />
      <InputForm
        inputValue={inputValue}
        changeInput={changeInput}
        clearInput={clearInput}
        addTodo={addTodo}
      />
      <View style={styles.body}>
        <ToDo todo={todo} toggleTodo={toggleTodo} removeTodo={removeTodo} />
      </View>
    </View>
  );
};

export default TodoList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  header: {
    height: 40,
    backgroundColor: '#27AE60',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  infoText: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  inputPart: {
    height: 100,
    backgroundColor: 'white',
  },
  inputDiv: {
    flexDirection: 'row',
    marginRight: 10,
    marginVertical: 10,
  },
  inputTask: {
    height: 40,
    marginHorizontal: 10,
    backgroundColor: 'white',
    flex: 6,
    borderColor: 'grey',
    borderWidth: 0.5,
    borderRadius: 5,
    color: 'gray',
  },
  tombolInput: {
    width: 40,
    height: 40,
    flex: 1,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  tombolText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  itemSeparator: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  listEmpty: {
    fontSize: 20,
  },
  todoList: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  todoItemText: {
    fontSize: 14,
  },
  body: {
    marginTop: 10,
    flex: 1,
  },
});
