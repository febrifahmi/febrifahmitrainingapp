import React from 'react';
import {View, Text} from 'react-native';
import {TodoProvider} from '../src/context/context';
import TodoList from './TodoList';

const TodoApp = () => {
  const initialValue = [
    {
      id: 1,
      todo: 'Coba react native',
      completed: false,
    },
    {
      id: 2,
      todo: 'Coba react context',
      completed: false,
    },
  ];

  return (
    <TodoProvider value={initialValue}>
      <TodoList />
    </TodoProvider>
  );
};

export default TodoApp;
